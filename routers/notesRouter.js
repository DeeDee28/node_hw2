/* eslint new-cap: ["error", { "capIsNewExceptionPattern": "^express\.." }] */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('../routers/middlewares/authMiddleware');
const {validateText} = require('./middlewares/validationMiddleware');
const {
  addNote,
  getNotes,
  getNoteById,
  deleteNoteById,
  patchNoteById,
  changeNoteText,
} = require('../controllers/notesController');

router.get('/', authMiddleware, asyncWrapper(getNotes));
router.get('/:id', authMiddleware, asyncWrapper(getNoteById));
router.post('/', authMiddleware, asyncWrapper(validateText),
    asyncWrapper(addNote));

router.patch('/:id', authMiddleware, asyncWrapper(patchNoteById));
router.put('/:id', authMiddleware, asyncWrapper(validateText),
    asyncWrapper(changeNoteText));

router.delete('/:id', authMiddleware, asyncWrapper(deleteNoteById));

module.exports = router;
