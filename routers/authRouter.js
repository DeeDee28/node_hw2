/* eslint new-cap: ["error", { "capIsNewExceptionPattern": "^express\.." }] */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('./helpers');
const {
  validateRegistr,
  validateLogin,
} = require('./middlewares/validationMiddleware');
const {register, login} = require('../controllers/authController');

router.post('/register', asyncWrapper(validateRegistr), asyncWrapper(register));
router.post('/login', asyncWrapper(validateLogin), asyncWrapper(login));

module.exports = router;
