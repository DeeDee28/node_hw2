const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

const getMyProfile = async (req, res) => {
  const user = req.user;

  res.json({
    'user': {
      '_id': user._id,
      'username': user.username,
      'createdDate': user.createdDate,
    },
  });
};

const deleteMyProfile = async (req, res) => {
  const user = req.user;

  await User.deleteOne({username: user.username});

  res.json({message: 'Success'});
};

const patchMyProfile = async (req, res) => {
  const user = req.user;
  const {oldPassword, newPassword} = req.body;

  const oldUser = await User.findOne({username: user.username});

  if ( !(await bcrypt.compare(oldPassword, oldUser.password)) ) {
    return res.status(400).json({message: `Wrong old password`});
  }

  await User.updateOne(
      {username: user.username}, {password: await bcrypt.hash(newPassword, 10)},
  );

  res.json({message: 'Success'});
};

module.exports = {
  getMyProfile,
  deleteMyProfile,
  patchMyProfile,
};
