const {Note} = require('../models/noteModel');

const addNote = async (req, res) => {
  const {text} = req.body;
  const note = new Note({userId: req.user._id, text: text});

  await note.save();

  res.json({message: 'Success'});
};

const getNotes = async (req, res) => {
  const {offset = 0, limit = 0} = req.query;

  const notes = await Note.find(
      {userId: req.user._id},
      [],
      {
        skip: parseInt(offset),
        limit: parseInt(limit),
      },
  );

  res.json({notes: notes});
};

const getNoteById = async (req, res) => {
  const {id} = req.params;
  const note = await Note.findOne({userId: req.user._id, _id: id});

  if (!note) {
    return res.status(400).json({
      message: `You don't have a note with such an id`,
    });
  }

  res.json({note: note});
};

const deleteNoteById = async (req, res) => {
  const {id} = req.params;
  const note = await Note.findOne({userId: req.user._id, _id: id});

  if (!note) {
    return res.status(400).json({
      message: `You don't have a note with such an id`,
    });
  }

  await Note.deleteOne({userId: req.user._id, _id: id});

  res.json({message: 'Success'});
};

const patchNoteById = async (req, res) => {
  const {id} = req.params;
  const note = await Note.findOne({userId: req.user._id, _id: id});

  if (!note) {
    return res.status(400).json({
      message: `You don't have a note with such an id`,
    });
  }

  await Note.updateOne(
      {userId: req.user._id, _id: id},
      {completed: note.completed ? false : true},
  );

  res.json({message: 'Success'});
};

const changeNoteText = async (req, res) => {
  const {id} = req.params;
  const {text} = req.body;
  const note = await Note.findOne({userId: req.user._id, _id: id});

  if (!note) {
    return res.status(400).json({
      message: `You don't have a note with such an id`,
    });
  }

  await Note.updateOne({userId: req.user._id, _id: id}, {text: text});

  res.json({message: 'Success'});
};

module.exports = {
  addNote,
  getNotes,
  getNoteById,
  deleteNoteById,
  patchNoteById,
  changeNoteText,
};
