const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 8080;

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const notesRouter = require('./routers/notesRouter');

app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/notes', notesRouter);

app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

app.use('*', (req, res) => {
  res.status(404).json({message: 'Not Found'});
});

const start = async () => {
  await mongoose.connect('mongodb+srv://DeeDee28:ihOnb176oYzcE5x8@cluster0.zgkmb.mongodb.net/hw2?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  });

  app.listen(port, () => {
    console.log(`Server has started on port: ${port}`);
  });
};

start();
