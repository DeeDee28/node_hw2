const Joi = require('joi');

const validateRegistr = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .required(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  await schema.validateAsync(req.body);
  next();
};

const validateText = async (req, res, next) => {
  const schema = Joi.object({
    text: Joi.string()
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};

const validateLogin = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .required(),

    password: Joi.string()
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};

const validateChangePassword = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .required(),

    newPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports = {
  validateRegistr,
  validateLogin,
  validateText,
  validateChangePassword,
};
