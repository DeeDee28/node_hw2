/* eslint new-cap: ["error", { "capIsNewExceptionPattern": "^express\.." }] */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('./helpers');
const {validateChangePassword} = require('./middlewares/validationMiddleware');
const {
  getMyProfile,
  deleteMyProfile,
  patchMyProfile,
} = require('../controllers/userController');
const {
  authMiddleware,
} = require('../routers/middlewares/authMiddleware');

router.get('/me', authMiddleware, asyncWrapper(getMyProfile));
router.delete('/me', authMiddleware, asyncWrapper(deleteMyProfile));
router.patch('/me', authMiddleware, asyncWrapper(validateChangePassword),
    asyncWrapper(patchMyProfile));

module.exports = router;
